package com.notification.config;


import com.notification.messaging.streams.GenericStream;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@EnableBinding(GenericStream.class)
@Configuration
public class StreamsConfig {
}
