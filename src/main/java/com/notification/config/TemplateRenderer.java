package com.notification.config;


import org.apache.velocity.app.VelocityEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TemplateRenderer {

    @Bean
    public VelocityEngine velocityEngine(){
        VelocityEngine velocity = new VelocityEngine();
        velocity.init();
        return velocity;
    }

}
