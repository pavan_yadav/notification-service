package com.notification.dto;

import lombok.Data;

@Data
public class Order{
    private String orderStatus;
    private String orderId;
    private String customerId;
}
