package com.notification.messaging.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface GenericStream {

    String ORDER_STATUS_INPUT = "order-status-in";

    @Input(ORDER_STATUS_INPUT)
    SubscribableChannel inboundStockUpdate();
}
