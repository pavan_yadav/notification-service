package com.notification.messaging.listener;

import com.notification.messaging.events.NotificationEvent;
import com.notification.messaging.streams.GenericStream;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
@Log4j2
public class NotificationListener {

    @StreamListener(value = GenericStream.ORDER_STATUS_INPUT)
    public void handleNotificatoionUpdate(@Valid Message<NotificationEvent> notificationEventMessage){

    }
}
