package com.notification.messaging.events;

import com.notification.dto.Order;
import lombok.Data;

@Data
public class NotificationEvent {
    private String eventType;
    private Order order;
}
