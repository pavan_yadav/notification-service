package com.notification.api;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.notification.config.TemplateRenderer;
import lombok.extern.log4j.Log4j2;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.util.*;

@Controller
@Log4j2
public class Api {

    @Autowired
    private VelocityEngine velocityEngine;

    private MimeMessage msg;
    private String subject="Sample Email";
    private String sender="no-reply@licious.in";
    private String[] recipients = new String[]{"pavanyadav@licious.com"};

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");

    @GetMapping("velocity")
    public ResponseEntity<String> Velocity(){
        String result=null;
        Template template = velocityEngine.getTemplate("src/main/resources/templates/welcome.vm");
        VelocityContext context = new VelocityContext();


        context.put("products", "hello");
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        result = writer.toString();

        System.out.println("result "+result);

        return new ResponseEntity<>(result,HttpStatus.OK);
    }

//    @GetMapping("/")
//    public String main(Model model) {
//        model.addAttribute("message", message);
//        model.addAttribute("tasks", tasks);
//
//        return "welcome"; //view
//    }

    // /hello?name=kotlin
//    @GetMapping("/hello")
//    public String mainWithParam(
//            @RequestParam(name = "name", required = false, defaultValue = "")
//                    String name, Model model) {
//
//        model.addAttribute("message", name);
//
//        return "welcome"; //view
//    }

    @GetMapping("/mail")
    public ResponseEntity<Map<String,String>> mail() throws MessagingException, javax.mail.MessagingException, IOException {
        setEmailConfigs();
        setEmailContent();
        sendEmail();
        Map<String,String> mp = new HashMap<>();
        mp.put("message","Email Sent...");
        return new ResponseEntity<>(mp, HttpStatus.OK);
    }

    private void setEmailConfigs() throws javax.mail.MessagingException {
        JavaMailSender emailSender = new JavaMailSenderImpl();
        msg = emailSender.createMimeMessage();
        msg.setSubject(subject, "UTF-8");
        msg.setFrom(new InternetAddress(sender));
        InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];
        int addressCount = 0;
        for (String recipient : recipients) {
            recipientAddresses[addressCount] = new InternetAddress(recipient.trim());
            addressCount++;
        }
        msg.setRecipients(Message.RecipientType.TO, recipientAddresses);
    }

    private void setEmailContent() throws MessagingException {
        // Create a multipart/alternative child container.
        MimeMultipart msg_body = new MimeMultipart("alternative");
        // Create a wrapper for the HTML.
        MimeBodyPart wrap = new MimeBodyPart();
        // Define the HTML part.
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent("hello","text/html; charset=UTF-8");
        // Add the HTML part to the child container.
        msg_body.addBodyPart(htmlPart);
        // Add the child container to the wrapper object.
        wrap.setContent(msg_body);
        // Create a multipart/mixed parent container.
        MimeMultipart mimeMultipart = new MimeMultipart("mixed");
        // Add the parent container to the message.
        msg.setContent(mimeMultipart);
        // Add the multipart/alternative part to the message.
//        mimeMultipart.addBodyPart(wrap);​
        mimeMultipart.addBodyPart(wrap);
    }
    private void sendEmail() throws IOException, MessagingException {
        // Try to send the email.
        try {
//            log.info("Attempting to send an email through Amazon SES "
//                    + "using the AWS SDK for Java...");​
            log.info("hi");
            // Instantiate an Amazon SES client, which will make the service
            // call with the supplied AWS credentials.
            String ACCESS_KEY="AKIAQNLOUMQ5RHO3RCVL",
            SECRET_KEY="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";

            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            .withRegion(Regions.US_WEST_2).build();
            // Send the email.
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            msg.writeTo(outputStream);
            RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
            SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
            client.sendRawEmail(rawEmailRequest);
            log.info("Email sent!");
            // Display an error if something goes wrong.
        } catch (IOException ex) {
            log.error("Email Failed");
            log.error("Error message: " + ex.getMessage());
            throw ex;
        }
    }
}
